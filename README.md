# Exemptions

Create a file named `dependency-exemptions.yml` in the root of your project. The content should be an array of objects containing package names to exempt.

```yaml
- name: package/name
```

You can exempt specific versions of a package using a version regex:

```yaml
- name: package/name
  version: 1\.2\..*
```

## Testing

```shell
docker run -it --rm -v $(pwd):/opt/project -v /path/to/mu-php-checker:/opt/checker miamioh/php:7.3-devtools bash

php /opt/checker/bin/console check:outdated-dependencies --direct
```