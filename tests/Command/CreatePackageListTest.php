<?php

namespace Tests\Command;

use App\Command\CreatePackageList;
use Illuminate\Support\Collection;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Tester\CommandTester;
use PHPUnit\Framework\MockObject\MockObject;
use App\Composer\PackageLoader;


/**
 * @property CommandTester $commandTester
 */
class CreatePackageListTest extends KernelTestCase
{
    /** @var PackageLoader|MockObject */
    private $packageLoaderMock;
    /** @var CommandTester */
    private $commandTester;

    protected function setup():void
    {
        $this->packageLoaderMock = $this->getMockBuilder(PackageLoader::class)
            ->disableOriginalConstructor()
            ->getMock();
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $application->add(new CreatePackageList($this->packageLoaderMock));
        $this->command = $application->find('create:package-list');
        $this->commandTester = new CommandTester($this->command);
    }

    protected function tearDown():void
    {
        $this->packageLoaderMock = null;
        $this->commandTester = null;
    }

    public function testCommandRuns()
    {
        $package1 = array(
            'name'=> 'laravel/laravel',
            'version' => '2.0.6',
            'description' => 'Test Package.'
        );

        $packages = new Collection([$package1]);
        $this->packageLoaderMock
            ->expects($this->once())
            ->method('getInstalledPackageList')
            ->willReturn($packages);


//        $this->commandTester->execute(['command' => $this->command->getName()]);
        $this->commandTester->execute([]);
        $this->commandTester->assertCommandIsSuccessful();
    }

    public function testCanGenerateListOfPackages()
    {
        $package1 = array(
            'name'=> 'symfony/console',
            'version' => '2.0.6',
            'description' => 'Test Package.'
        );
        $package2 = array(
            'name'=> 'laravel/laravel',
            'version' => '2.0.6',
            'description' => 'Test Package.'
        );

        $packages = new Collection([$package1,$package2]);
        $this->packageLoaderMock
            ->expects($this->once())
            ->method('getInstalledPackageList')
            ->willReturn($packages);


        $this->commandTester->execute(['command' => $this->command->getName()],[]);
        $output = $this->commandTester->getDisplay();
        $this->assertStringContainsString('symfony/console',$output);
    }

}
