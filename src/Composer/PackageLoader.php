<?php


namespace App\Composer;


use App\Exception\PackageTypeException;
use App\Exception\PhpCheckerException;
use Illuminate\Support\Collection;
use Symfony\Component\Process\Process;

class PackageLoader
{
    /** @var Collection */
    private $packages;

    /** @var Collection */
    private $filteredPackages;

    /** @var string */
    private $packageType;

    private $json = '';

    /**
     * @param string|null $projectPath
     * @throws PhpCheckerException
     */
    public function loadInstalledPackages(?string $projectPath): void
    {
        $this->loadPackages(['composer', 'show', '-f', 'json'], $projectPath);
        $this->packageType = 'installed';
    }

    /**
     * @param string|null $projectPath
     * @param bool $excludeDev
     * @throws PhpCheckerException
     */
    public function loadDirectOutdatedPackages(?string $projectPath, bool $excludeDev = false): void
    {
        $command = ['composer', 'show', '-f', 'json', '-lomD'];

        if ($excludeDev) {
            $command[] = '--no-dev';
        }

        $this->loadPackages($command, $projectPath);
        $this->packageType = 'outdated';
    }

    /**
     * @param string|null $projectPath
     * @throws PhpCheckerException
     */
    public function loadDirectOutdatedPackagesNoDev(?string $projectPath): void
    {
        $this->loadDirectOutdatedPackages($projectPath, true);
    }

    /**
     * @param string|null $projectPath
     * @param bool $excludeDev
     * @throws PhpCheckerException
     */
    public function loadOutdatedPackages(?string $projectPath, bool $excludeDev = false): void
    {
        $command = ['composer', 'show', '-f', 'json', '-lom'];

        if ($excludeDev) {
            $command[] = '--no-dev';
        }

        $this->loadPackages($command, $projectPath);
        $this->packageType = 'outdated';
    }

    /**
     * @param string|null $projectPath
     * @throws PhpCheckerException
     */
    public function loadOutdatedPackagesNoDev(?string $projectPath): void
    {
        $this->loadOutdatedPackages($projectPath, true);
    }

    /**
     * @param array $filters
     * @return $this
     */
    public function filter(array $filters): self
    {
        $this->filteredPackages = new Collection();

        foreach ($filters as $filter) {
            if (empty($filter['name'])) {
                throw new \InvalidArgumentException('Filter is missing "name" entry');
            }

            $this->packages = $this->packages->filter(function (array $package) use ($filter) {
                if ($package['name'] !== $filter['name']) {
                    return true;
                }

                if (empty($filter['version'])) {
                    if ($package['name'] === $filter['name']) {
                        $this->filteredPackages->add($package);
                        return false;
                    }

                    return true;
                }

                if ($package['version'] === $filter['version']) {
                    $this->filteredPackages->add($package);
                    return false;
                }

                return true;
            });
        }

        return $this;
    }

    public function filteredPackages(): Collection
    {
        if ($this->filteredPackages === null) {
            return new Collection();
        }

        return $this->filteredPackages;
    }

    /**
     * @return Collection
     * @throws PackageTypeException
     */
    public function packages(): Collection
    {
        $this->ensurePackages();

        return $this->packages;
    }

    /**
     * @param string $name
     * @return bool
     * @throws PackageTypeException
     */
    public function packageIsInstalled(string $name): bool
    {
        $this->ensureInstalledPackages();

        return $this->hasPackage($name);
    }

    /**
     * @param string $name
     * @return array|null
     * @throws PackageTypeException
     */
    public function findInstalledPackage(string $name): ?array
    {
        $this->ensureInstalledPackages();

        return $this->findPackage($name);
    }

    /**
     * @return Collection
     * @throws PackageTypeException
     */
    public function outdatedPackages(): Collection
    {
        $this->ensureOutdatedPackages();

        return $this->packages;
    }

    /**
     * @return Collection
     * @throws PackageTypeException
     */
    public function packagesWithWarnings(): Collection
    {
        $this->ensureOutdatedPackages();

        return $this->packages->whereNotNull('warning');
    }

    /**
     * @throws PackageTypeException
     */
    public function getInstalledPackageList(): Collection
    {
        $this->ensurePackages();
        return $this->packages;
    }

    /**
     * @return Collection
     * @throws PackageTypeException
     */
    public function packagesWithSafeUpdates(): Collection
    {
        $this->ensureOutdatedPackages();

        return $this->packages->where('latest-status', '=', 'semver-safe-update');
    }

    public function lastJson(): string
    {
        return $this->json;
    }

    /**
     * @param string $name
     * @return bool
     * @throws PackageTypeException
     */
    private function hasPackage(string $name): bool
    {
        $this->ensurePackages();

        return null !== $this->packages->firstWhere('name', '=', $name);
    }

    /**
     * @param string $name
     * @return array|null
     * @throws PackageTypeException
     */
    private function findPackage(string $name): ?array
    {
        $this->ensurePackages();

        return $this->packages->firstWhere('name', '=', $name);
    }

    /**
     * @param array $command
     * @param string|null $projectPath
     * @throws PhpCheckerException
     */
    private function loadPackages(array $command, ?string $projectPath): void
    {
        $process = new Process($command, $projectPath);
        $process->setTimeout(300);
        $process->run();

        $this->json = $process->getOutput();

        if (empty($this->json)) {
            throw new PhpCheckerException($process->getErrorOutput());
        }

        try {
            $packageData = json_decode($this->json, true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            throw new PhpCheckerException('Failed to load packages from composer', $e->getCode(), $e);
        }

        $this->getInstalledPackages($packageData);
    }

    private function getInstalledPackages(array $packageData): void
    {
        if (empty($packageData['installed'])) {
            $this->packages = new Collection();
            return;
        }

        $this->packages = new Collection($packageData['installed']);
    }

    /**
     * @throws PackageTypeException
     */
    private function ensurePackages(): void
    {
        if (null !== $this->packageType) {
            return;
        }

        throw new PackageTypeException('Packages have not been loaded');
    }

    /**
     * @throws PackageTypeException
     */
    private function ensureInstalledPackages(): void
    {
        if ($this->packageType === 'installed') {
            return;
        }

        throw new PackageTypeException('Installed packages have not been loaded');
    }

    private function ensureOutdatedPackages(): void
    {
        if ($this->packageType === 'outdated') {
            return;
        }

        throw new PackageTypeException('Outdated packages have not been loaded');
    }
}