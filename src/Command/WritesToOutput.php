<?php


namespace App\Command;


use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

trait WritesToOutput
{
    /** @var ConsoleOutputInterface */
    protected $output;

    protected function useOutputChannel(OutputInterface $output): void
    {
        if (!$output instanceof ConsoleOutputInterface) {
            throw new \LogicException('This command accepts only an instance of "ConsoleOutputInterface".');
        }

        $this->output = $output;
    }

    protected function writeHeading(string $heading): void
    {
        $borderRow = str_repeat('#', strlen(sprintf('############ %s #############', $heading)));

        $this->output->writeln(sprintf('<fg=green;options=bold>%s</>', $borderRow));
        $this->output->writeln(sprintf('<fg=green;options=bold>############ %s #############</>', $heading));
        $this->output->writeln(sprintf('<fg=green;options=bold>%s</>', $borderRow));
    }

    protected function writeInfo(string $message): void
    {
        $this->output->writeln(sprintf('<info>%s</info>', $message));
    }

    protected function writeError(string $message): void
    {
        $this->output->writeln(sprintf('<error>%s</error>', $message));
    }

    protected function writeComment(string $message): void
    {
        $this->output->writeln(sprintf('<comment>%s</comment>', $message));
    }

    protected function write(string $message): void
    {
        $this->output->writeln($message);
    }

    protected function table(array $headers, array $data): void
    {
        $table = new Table($this->output);

        $table->setHeaders($headers)->setRows($data);

        $table->render();
    }
}