<?php

namespace App\Command;

use App\Composer\PackageLoader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckLaravelMinimumRelease extends Command
{
    use WritesToOutput;

    public const LARAVEL_PACKAGE_NAME = 'laravel/framework';

    public const MINIMUM_VERSION = '9.0.0';

    protected static $defaultName = 'check:laravel-version';

    /**
     * @var PackageLoader
     */
    private $packageLoader;

    public function __construct(PackageLoader $packageLoader)
    {
        parent::__construct();

        $this->packageLoader = $packageLoader;
    }

    protected function configure()
    {
        $this->setDescription('Checks that minimum allowed version of Laravel is used.');

        $this->setHelp('Provide a path to a directory containing a composer.json file.');

        $this->addArgument('directory', InputArgument::OPTIONAL, 'Project directory');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->useOutputChannel($output);

        $this->packageLoader->loadInstalledPackages($input->getArgument('directory'));

        $laravel = $this->packageLoader->findInstalledPackage(self::LARAVEL_PACKAGE_NAME);

        $this->writeHeading('Laravel');

        if (empty($laravel)) {
            $this->writeError('Laravel framework is not used in this application.');
            return Command::FAILURE;
        }

        $installedVersion = str_replace('v', '', $laravel['version']);

        $passMinimum = version_compare($installedVersion, self::MINIMUM_VERSION, '>=');

        if ($passMinimum) {
            $this->writeInfo(sprintf('Laravel version %s is at or above the required minimum %s', $installedVersion, self::MINIMUM_VERSION));
            return Command::SUCCESS;
        }

        $this->writeError(sprintf('Laravel version %s is below the required minimum %s', $installedVersion, self::MINIMUM_VERSION));
        return Command::FAILURE;
    }
}
