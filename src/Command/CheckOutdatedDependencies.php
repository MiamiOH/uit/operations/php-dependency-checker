<?php

namespace App\Command;

use App\Composer\PackageLoader;
use App\Exception\PhpCheckerException;
use Illuminate\Support\Collection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CheckOutdatedDependencies extends Command
{
    use WritesToOutput;

    public const EXEMPTION_FILE = './dependency-exemptions.yml';

    protected static $defaultName = 'check:outdated-dependencies';

    /**
     * @var PackageLoader
     */
    private $packageLoader;

    public function __construct(PackageLoader $packageLoader)
    {
        parent::__construct();

        $this->packageLoader = $packageLoader;
    }

    protected function configure()
    {
        $this->setDescription('Checks for outdated project dependencies.');

        $this->setHelp('Provide a path to a directory containing a composer.json file.');

        $this->addArgument('directory', InputArgument::OPTIONAL, 'Project directory');

        $this->addOption('direct', 'd', InputOption::VALUE_NONE, 'Direct dependencies only');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->useOutputChannel($output);

        try {
            if ($input->getOption('direct')) {
                $this->packageLoader->loadDirectOutdatedPackagesNoDev($input->getArgument('directory'));
            } else {
                $this->packageLoader->loadOutdatedPackagesNoDev($input->getArgument('directory'));
            }
        } catch (PhpCheckerException $e) {
            $this->writeError(sprintf('Error: %s', $e->getMessage()));
            $this->write($this->packageLoader->lastJson());
            return Command::FAILURE;
        }

        $filter = $this->getFilters();

        $this->packageLoader->filter($filter);
        $filtered = $this->packageLoader->filteredPackages();

        if ($filtered->isNotEmpty()) {
            $this->writeHeading('Exempt Dependencies');
            $this->writeError(sprintf('%s package(s) are allowed to be outdated', $filtered->count()));
            $this->packageTable($filtered);
        }

        $outdated = $this->packageLoader->outdatedPackages();
        $this->writeHeading('Outdated Dependencies');

        if ($outdated->isEmpty()) {
            $this->writeInfo('All dependency packages are up to date.');
            return Command::SUCCESS;
        }

        $warnings = $this->packageLoader->packagesWithWarnings();

        if ($warnings->isNotEmpty()) {
            $this->writeError(sprintf('%s package(s) have warnings which should be resolved', $warnings->count()));
            $this->packageTable($warnings);
        }

        $updates = $this->packageLoader->packagesWithSafeUpdates();

        if ($updates->isNotEmpty()) {
            $this->writeError(sprintf('%s package(s) have updates which can be applied', $updates->count()));
            $this->packageTable($updates);
            $this->writeComment(sprintf('Add packages to %s to exempt', self::EXEMPTION_FILE));
            $this->writeComment(sprintf('Run `composer update %s -W` to update these',
                $updates->pluck('name')->join(' ')));
        }

        if ($warnings->isEmpty() && $updates->isEmpty()) {
            $this->packageTable($outdated);
        }

        $this->writeComment('Run `composer show -lomD --no-dev` to view outdated direct dependencies (remove the -D option to view all)');

        return Command::FAILURE;
    }

    private function packageTable(Collection $packages): void
    {
        $headers = [
            'Name',
            'Version',
            'Latest'
        ];

        $rows = $packages->reduce(function (array $c, array $package) {
            $c[] = [
                $package['name'],
                $package['version'],
                $package['latest'] ?? '',
            ];
            return $c;
        }, []);

        $this->table($headers, $rows);
    }

    private function getFilters(): array
    {
        $filters = [];

        if (!file_exists(self::EXEMPTION_FILE)) {
            return $filters;
        }

        $ndocs = 0;

        $filters = yaml_parse_file(self::EXEMPTION_FILE, 0, $ndocs,
            array(
                // if value is interpreted as a FLOAT return the value of the float as a string instead
                YAML_FLOAT_TAG => function ($value) {
                    return strval($value);
                }
            )
        );
        return $filters;
    }
}
