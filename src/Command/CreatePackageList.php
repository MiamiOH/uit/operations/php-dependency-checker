<?php

namespace App\Command;

use App\Composer\PackageLoader;
use App\Exception\PackageTypeException;
use App\Exception\PhpCheckerException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class CreatePackageList extends Command
{
    protected static $defaultName = 'create:package-list';
    protected static $defaultDescription = 'Create list of packages in composer.json';

    /**
     * @var PackageLoader
     */
    private $packageLoader;

    public function __construct(PackageLoader $packageLoader)
    {

        $this->packageLoader = $packageLoader;
        parent::__construct();
    }
    protected function configure(): void
    {
        $this->setHelp('Provide a path to a directory containing a composer.json file.');
        $this->addArgument('directory', InputArgument::OPTIONAL, 'Project directory');
        $this->addArgument('format', InputArgument::OPTIONAL, 'Output format (default formatted as a table)');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws PackageTypeException
     * @throws PhpCheckerException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $this->packageLoader->loadInstalledPackages($input->getArgument('directory'));
        $installedPackages = $this->packageLoader->getInstalledPackageList();


        if ($input->getArgument('format') == 'json') {
            $rows = $installedPackages->reduce(function (array $c, array $package) {
                $c[] = ['name' => $package['name'] , 'version' =>
                    $package['version'],
                ];
                return $c;
            }, []);
            $encoders = [new JsonEncoder()];
            $normalizers = [new ObjectNormalizer()];

            $serializer = new Serializer($normalizers, $encoders);

//            $response = new Json($rows);
            $response = $serializer->serialize($rows, 'json', ['json_encode_options' => \JSON_PRESERVE_ZERO_FRACTION]);

            $output->write($response);
        } else {
            $table = new Table($output);
            $table->setHeaders([
                'Name',
                'Version',
            ]);
            $rows = $installedPackages->reduce(function (array $c, array $package) {
                $c[] = [
                    $package['name'],
                    $package['version'],
                ];
                return $c;
            }, []);

            $table->setRows($rows);
            $table->render();
        }
        return Command::SUCCESS;
    }

}