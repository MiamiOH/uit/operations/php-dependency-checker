<?php

namespace App\Command;

use App\Composer\PackageLoader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckSecurityAdvisoriesIsPresent extends Command
{
    use WritesToOutput;

    public const SECURITY_ADVISORY_PACKAGE = 'roave/security-advisories';

    protected static $defaultName = 'check:security-advisories';

    /**
     * @var PackageLoader
     */
    private $packageLoader;

    public function __construct(PackageLoader $packageLoader)
    {
        parent::__construct();

        $this->packageLoader = $packageLoader;
    }

    protected function configure()
    {
        $this->setDescription(sprintf('Checks that %s is present', self::SECURITY_ADVISORY_PACKAGE));

        $this->setHelp('Provide a path to a directory containing a composer.json file.');

        $this->addArgument('directory', InputArgument::OPTIONAL, 'Project directory');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->useOutputChannel($output);

        $this->packageLoader->loadInstalledPackages($input->getArgument('directory'));

        $this->writeHeading('Advisories');

        if ($this->packageLoader->packageIsInstalled(self::SECURITY_ADVISORY_PACKAGE)) {
            $this->writeInfo(sprintf('%s is included with this package', self::SECURITY_ADVISORY_PACKAGE));
            return Command::SUCCESS;
        }

        $this->writeError(sprintf('%s is not included with this package', self::SECURITY_ADVISORY_PACKAGE));
        $this->writeComment(sprintf('Run `composer require --dev %s:dev-latest` to add it.', self::SECURITY_ADVISORY_PACKAGE));
        return Command::FAILURE;
    }
}
